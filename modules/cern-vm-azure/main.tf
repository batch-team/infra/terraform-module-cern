##
## Data Sources
##
data "local_file" "userdata" {
  filename = "${path.module}/userdata"
}

##
## Resources
##
resource "azurerm_network_interface" "eth" {
  name                = local.net_interface_name
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "ipconfig"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
  }
  tags = var.tags
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                  = local.vm_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.eth.id]
  size                  = var.size

  /* Spot related settings */
  priority        = var.priority
  max_bid_price   = local.max_bid_price
  eviction_policy = local.eviction_policy

  source_image_id = var.source_image_id
  dynamic "source_image_reference" {
    for_each = local.dyn_source_image_ref
    content {
      publisher = var.source_image_reference["publisher"]
      offer     = var.source_image_reference["offer"]
      sku       = var.source_image_reference["sku"]
      version   = var.source_image_reference["version"]
    }
  }

  os_disk {
    name                 = local.osdisk_name
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = var.os_disk_size_gb
  }

  admin_username                  = var.username
  disable_password_authentication = true
  admin_ssh_key {
    username   = var.username
    public_key = var.ssh_public_key
  }

  computer_name = var.name
  custom_data   = data.local_file.userdata.content_base64

  lifecycle {
    ignore_changes = [
      custom_data,
      source_image_reference,
      source_image_id,
    ]
  }

  tags = var.tags

  provisioner "local-exec" {
    when    = destroy
    command = "/usr/bin/ai-kill --nova-disable ${self.computer_name}.cern.ch"
  }

  provisioner "local-exec" {
    command = "/bin/sleep $[ ( $RANDOM % 5 )  + 1 ]s"
  }

  provisioner "local-exec" {
    command = "/usr/bin/ai-bs --foreman-environment ${var.puppet_environment} --roger-appstate ${var.roger_appstate} --nova-disable -i \"a fake image\" -g ${var.hostgroup} ${var.name}.cern.ch"
  }
}

#
# REGISTER THE NEW VM IN LANDB
#
resource "cern_landb_vm" "landb_machine" {
  device_name = var.name
  location = {
    building = "0000"
    floor    = "0"
    room     = "0000"
  }
  manufacturer = "HYPER-V"
  model        = "VIRTUAL MACHINE"
  description  = "Azure Cloud Virtual Machine"
  tag          = "AZURE CLOUD VM"
  operating_system = {
    name    = "LINUX"
    version = "UNKNOWN"
  }
  landb_manager_person = var.landb_manager
  responsible_person   = var.landb_responsible
  user_person          = var.landb_user
  ipv6_ready           = false
  manager_locked       = false
}

# TODO: Explain why this.


data "azurerm_network_interface" "nic" {
  name                = local.net_interface_name
  resource_group_name = var.resource_group_name
  depends_on          = [azurerm_linux_virtual_machine.vm]
}
resource "null_resource" "trigger-hook" {
  triggers = {
    vm_id = azurerm_linux_virtual_machine.vm.id
  }
}

resource "cern_landb_vm_card" "landb_machine_card" {
  vm_name          = cern_landb_vm.landb_machine.id
  hardware_address = data.azurerm_network_interface.nic.mac_address
  card_type        = "Ethernet"
  lifecycle {
    ignore_changes = [
      hardware_address
    ]
  }
}

resource "cern_landb_vm_interface" "landb_machine_interface" {
  vm_name          = cern_landb_vm_card.landb_machine_card.vm_name
  interface_domain = "cern.ch"
  vm_cluster_name  = var.landb_cluster_name
  vm_interface_options = {
    ip           = azurerm_network_interface.eth.private_ip_address
    service_name = var.landb_service_name
    address_type = var.landb_address_type
  }
}
