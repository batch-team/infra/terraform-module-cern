terraform {
  required_version = ">= 0.13"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.38.0"
    }
    cern = {
      source  = "gitlab.cern.ch/batch-team/cern"
      version = "1.1.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.0.0"
    }
  }
}
