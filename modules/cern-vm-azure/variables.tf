# Common
variable "name" {
  description = "Virtual machine hostname"
}

# Azure
variable "ssh_public_key" {
  description = "File containing the SSH public key to inject"
}

variable "location" {
  description = "Azure region where the resources will be created. Stick to 'West Europe'."
}

variable "username" {
  description = "The username of the local administrator used for the Virtual Machine. Changing this forces a new resource to be created."
}

variable "subnet_id" {
}

variable "size" {
  description = "The Azure SKU which should be used for this Virtual Machine."
}

variable "os_disk_size_gb" {
  default = null
}

variable "priority" {
  default     = "Regular"
  description = "Specifies the priority of this Virtual Machine. Possible values are Regular and Spot. Defaults to Regular."
}

variable "max_bid_price" {
  default     = -1
  type        = number
  description = "The maximum price you're willing to pay for this Virtual Machine, in US Dollars"
}

variable "resource_group_name" {
  description = "The name of the Resource Group in which the Virtual Machine should be exist. Changing this forces a new resource to be created."
}

variable "tags" {
  description = "A mapping of tags which should be assigned to this Virtual Machine"
}

variable "source_image_id" {
  default = null
}

variable "source_image_reference" {
  default = {
    publisher = "OpenLogic"
    offer     = "CentOS-CI"
    sku       = "7-CI"
    version   = "7.7.20191209"
  }

  description = "An image reference map containing publisher, offer, sku and version. Changing this forces a new resource to be created."
}

# CERN
variable "hostgroup" {
  description = "Foreman hostgroup to place the virtual machines in"
}

variable "puppet_environment" {
  default = "production"
}

variable "roger_appstate" {
  default = "build"
}

variable "landb_responsible" { }
variable "landb_user" { }
variable "landb_manager" { }
variable "landb_cluster_name" { }
variable "landb_service_name" { }
variable "landb_address_type" { }

# Local variables
locals {
  net_interface_name     = "nic-${var.name}"
  vm_name                = "vm-${var.name}"
  osdisk_name            = "osdisk-${var.name}"

  eviction_policy        = var.priority == "Spot" ? "Deallocate" : null
  max_bid_price          = var.priority == "Spot" ? var.max_bid_price : null

  # Use the ID if set, otherwise use the reference.
  dyn_source_image_ref   = var.source_image_id == null ? [1] : []
}